<?php

/**
 * Implements hook_easy_extra_fields().
 *
 * Define your extra fields, slightly different from hook_field_extra_fields().
 */
function hook_easy_extra_fields() {
  // The simplest variant: only a 'name', so structure optional. Will use
  // defaults as stated below.
  $fields['node']['page']['foo'] = t('My simple field');

  // The same simple, but with normal structure.
  $fields['node']['page']['foo'] = array(
    // Mandatory.
    'name' => t('My simple field'),

    // Optional, a custom callback. You can reuse this across fields and bundles and entity types. If you
    // don't include a 'callback', we will call MODULE_eef__node__page__foo().
    'callback' => 'mymodule_eef_entity_title',

    // Optional, below is default. Will be passed to the callback.
    'variables' => array(),

    // Optional, below is default. If TRUE, will wrap the callback result in a fake field.
    'field_wrap' => TRUE,
  );

  return $fields;
}

/**
 * EEF callback for node.page.foo.
 */
function MODULE_eef__node__page__foo($node, $vars, $definition) {
  return drupal_strlen($node->title) * $vars['factor'];
}

/**
 * EEF callback for all entity titles.
 */
function mymodule_eef_entity_title($entity, $vars, $definition) {
  return entity_label($definition['entity_type'], $entity);
}
